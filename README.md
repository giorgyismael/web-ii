# Leiame #

Este Projeto tem por objetivo demonstrar boas práticas na produção de páginas web, bem como um tutorial de como deve ser feita o desenvolvimento utilizando o framework Django 1.7 acompanhado da linguagem de programação Python 2.7

### Instalação Ambiente Virtual ###

Item Ativando o Ambiente de trabalho Virtual
Para ativar o ambiente virtual, primeiramente é necessário realizar a instalação do virtualenv, jutamente com o virtualwapper, lembrando que ao instalar é necessári instalar o Pip e Setuptools que é o gerenciador de pacotes normalmente utilizado para promover a instalação de programas em sue ambiente virtual.

* Vamos aos passos
Instalando Pip e setuptools

> sudo apt-get install python-pip ou
> sudo apt-get install python3-pip

* Nota: Que quiser executar com python três, é necessári instalar o pip para python 3, desta forma, basta promover o seguinte comando.



* Instalar o o ambiente Virtual, basta fazer o comando abaixo
> sudo apt-get install python-virtualenv
> sudo apt-get install virtualwrapper



### Ativando Ambiente Virtual ###

* Para criar um ambiente virtual, basta proceder o seguinte comando
> mkvirtualenv [nome do projeto]

* Para Ativar o ambiente
> workon [nome projeto]

* Desativar
> deactivate

### Instalação de Pacotes e extençoes no ambiente ###

* Uma necessidade para esse projeto é Gerenciador de imagens Python e conhecido como Pillow, para instalar, basta realizar o seguinte comando com o ambiente virtual ligado
> (ambientevirtual) pip install pillow


### Lista de Programas ###
É importante manter uma lista atualizada dos requerimentos que um determinado projeto precisa, como o correto é criar um ambiente virtual para cada projeto e instlar os pacotes particulares em seus embientes, é prudente manter essa lista com os pacotes instalados, até para facilitar a entreda de outros conponentes na equipe. Para fazer isso basta proceder o seguinte comando

> pip freeze >> requiriments.txt

### Ferramentas de Trabalho ###
* Pycharm - Se puder resumir em uma palavra, é um sonho apra quem programa em Python para Web.
Cuida tudo para você, desde o ambiente virtual, até o repositorio Git. Então basta deixar tudo bem configurado.

* Local para Deploy - do Projeto.Utilizamos o pythonanywhare para tornar publico os projetos de forma gratuita

* Bitbocket - Utilizado para repositório e controle da atualização dos arquivos
