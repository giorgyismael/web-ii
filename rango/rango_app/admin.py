from django.contrib import admin
from rango_app.models import Categoria, Pagina, Usuario

class  PageAdmin(admin.ModelAdmin):
    list_display = ('categoria','titulo', 'url', "views")
    ordering = ("-views",)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ("nome", "views", "like")
    ordering = ("-like",)
    prepopulated_fields = {"slug":("nome", )}

admin.site.register(Categoria, CategoryAdmin)
admin.site.register(Pagina, PageAdmin)
admin.site.register(Usuario)

