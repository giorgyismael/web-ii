#coding:utf-8
from django import forms
from rango_app.models import Categoria


class FormCategoria(forms.ModelForm):
    nome = forms.CharField(max_length=128, help_text="Insira o nome da Categoria" )
    views = forms.IntegerField(widget=forms.HiddenInput, initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput, initial=0)
    slug = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Categoria
        fields = ("nome",)