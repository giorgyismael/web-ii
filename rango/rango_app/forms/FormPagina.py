#coding:utf-8
from django import forms
from rango_app.models import Pagina


class FormPagina(forms.ModelForm):
    #categoria = forms.ModelChoiceField(help_text="Categoria", queryset=Categoria.objects.all())
    titulo = forms.CharField(max_length=128, help_text='Informe titulo da Página')
    url = forms.URLField(max_length=200, help_text='Informe URL')
    views = forms.IntegerField(widget=forms.HiddenInput, initial=0, required=False)

    # def clean(self):
    #     cleaned_data = self.cleaned_data
    #     url = cleaned_data.get('url')
    #
    #     if url and not url.startswith('http://'):
    #         url = 'http://' + url
    #     cleaned_data['url'] = url
    #     return cleaned_data

    class Meta:
        model = Pagina
        fields = ('categoria','titulo','url', )
