#coding:utf-8
from django import forms
from rango_app.models import Usuario

class FormUsuarioComplemento(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('website', 'imagem')