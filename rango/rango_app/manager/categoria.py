#coding:utf-8
from django.db.models.manager import Manager

class CategoriaManager(Manager):

    def search(self, atributo):
        return self.get(slug=atributo)