from django.db import models
from django.template.defaultfilters import slugify
from rango_app.manager.categoria import CategoriaManager


class Categoria(models.Model):
    nome = models.CharField(max_length=128, unique=True)
    views = models.IntegerField(default=0)
    like = models.IntegerField(default=0)
    slug = models.SlugField(unique=True)

    objects = CategoriaManager()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nome)
        super(Categoria, self).save(*args, **kwargs)

    # def search(self, categoria_nome_bonito):
    #     return Categoria.objects.get(slug=categoria_nome_bonito)

    def __unicode__(self):
        return self.nome
