from django.db import models
from rango_app.models.categoria import Categoria
from rango_app.manager.pagina import PaginaManager

class Pagina(models.Model):
    categoria = models.ForeignKey(Categoria)
    titulo = models.CharField(max_length=128)
    url = models.URLField()
    views = models.IntegerField(default=0)

    objects = PaginaManager()

    def __unicode__(self):
        return self.titulo

    #def search(self, categoria):
    #    return Pagina.objects.filter(categoria=categoria)

    #def search(self, categoria_id):
    #   return Pagina.objects.filter(categoria_id=categoria_id).all()

    #def search(self, slugcategoria):
    #    return Pagina.objects.filter(categoria__slug=slugcategoria).all()