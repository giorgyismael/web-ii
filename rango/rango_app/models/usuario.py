from django.contrib.auth.models import User
from django.db import models


class Usuario(models.Model):
    usuario = models.OneToOneField(User)
    website = models.URLField(blank=True)
    imagem = models.ImageField(upload_to="profile_imagens", blank=True)

    def __unicode__(self):
        return self.usuario.username