from django.conf.urls import patterns, url
from rango_app import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^teste', views.teste, name='teste'),

    url(r'^about/', views.about, name='about'),
    url(r'^registro/$', views.registro, name='registro'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^restricted/', views.restricted, name='restricted'),
    url(r'^logout/$', views.user_logout, name='logout'),

    url(r'^add_categoria/', views.add_categoria, name='add_categoria'),

    url(r'^add_pagina/(?P<slug>[\w\-]+)/$', views.add_pagina, name='add2_pagina'),
    url(r'^add_pagina/', views.add_pagina, name='add_pagina'),

    url(r'^categoria/(?P<slug>[\w\-]+)/$', views.categoria, name='categoria'),)

