from rango_app.views.about import about
from rango_app.views.add_categoria import add_categoria
from rango_app.views.add_pagina import add_pagina
from rango_app.views.categoria import categoria
from rango_app.views.index import index
from rango_app.views.registro import registro
from rango_app.views.restricted import restricted
from rango_app.views.user_login import user_login
from rango_app.views.user_logout import user_logout
from rango_app.views.teste import teste






