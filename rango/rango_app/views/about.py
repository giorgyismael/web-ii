#coding:utf-8

from django.shortcuts import render

def about(request):

    #Teste de Cookie
    #request.session.set_test_cookie()
    #if request.session.test_cookie_worked():
       #print("Session funcionando")
    #request.session.delete_test_cookie()

    if request.session.get('visits'):
        visits = request.session.get('visits')
    else:
        visits = 0

    template = 'rango/about.html'
    conteudo = {
        'titulo_pagina':'Rango About',
        'titulo_conteudo':'Pagina About',
        'conteudo': 'Descrição do Rango é um baita desenho repleto de [...]',
        'visits': visits
    }

    return render(request, template, conteudo)