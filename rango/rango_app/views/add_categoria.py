#coding:utf-8

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rango_app.forms import FormCategoria
from rango_app.models import Categoria
from rango_app.views.get_visits import get_visitante

@login_required
def add_categoria(request):
    template = 'rango/add_categoria.html'

    if request.method != "POST":
        form = FormCategoria
    else:
        form = FormCategoria(request.POST)

        if form.is_valid():
            form.save()
            return index(request)

    conteudo = {
        "titulos_h1": ["Rango"],
        "titulos_h2": ["Adicionar Categoria"],
        "formulario": form,
        "categorias" : Categoria.objects.order_by("-like")[:5],
        'visits':get_visitante(request),
        'controle_menu':'cadastro',
    }
    return render(request, template, conteudo)
