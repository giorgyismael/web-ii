#coding:utf-8

from django.shortcuts import render, redirect
from rango_app.models import Categoria
from django.contrib.auth.decorators import login_required
from rango_app.forms import FormPagina
from rango_app.models import Categoria
from rango_app.views.get_visits import get_visitante

@login_required
def add_pagina(request, slug=None):
    template = 'rango/add_pagina.html'

    if request.method != "POST":
        form = FormPagina()

        if slug is not None:
            form.fields['categoria'].initial = Categoria.objects.search(slug)
            #form.fields['categoria'].widget.attrs['disabled'] = 'disabled'

    else:
        form = FormPagina(request.POST)
        if form.is_valid():
            pagina = form.save(commit=True)
            return redirect('categoria',slug=pagina.categoria.slug)

    conteudo = {
        "titulos_h1" : ["Rango"],
        "titulos_h2": ["Adicionar Pagina"],
        "formulario": form,
        "categorias" : Categoria.objects.order_by("-like")[:5],
        'visits':get_visitante(request),
        'controle_menu':'cadastro',
    }
    return render(request, template, conteudo)
