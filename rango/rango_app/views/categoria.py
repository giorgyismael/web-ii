#coding:utf-8

from django.shortcuts import render
from rango_app.models import Categoria, Pagina
from rango_app.views.get_visits import get_visitante

def categoria(request, slug):
    template = 'rango/categoria.html'
    conteudo = {}

    try:
        categoria = Categoria.objects.search(slug)
        paginas = Pagina.objects.search(categoria)
        conteudo = {
            'categoria': categoria,
            'paginas': paginas,
            "categorias" : Categoria.objects.order_by("-like")[:5],
            'visits':get_visitante(request),
            'controle_menu':categoria.nome,
        }
    except Categoria.DoesNotExist:
        pass


    return render(request, template, conteudo)