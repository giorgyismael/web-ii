#coding:utf-8
from datetime import datetime
from django.shortcuts import render
from rango_app.models import Categoria, Pagina

def index(request):

    template = 'rango/index.html'
    conteudo = {
    "categorias" : Categoria.objects.order_by("-like")[:5],
    "paginas" : Pagina.objects.order_by("-views")[:5],
    "titulo_pagina" : "Aplicação Django",
    "titulos_h1" : ["Rango"],
    "titulos_h2": ["Top 5 - Categorias", "Top 5 - Páginas"]
    }

    #GUARDANDO COOKIES


    # visits = int(request.COOKIES.get('visits', '1'))
    # conteudo['visits'] = visits
    #
    # reset_last_visit_time = False
    # #response = render(request, template, conteudo)
    # # Does the cookie last_visit exist?
    # if 'last_visit' in request.COOKIES:
    #     # Yes it does! Get the cookie's value.
    #     last_visit = request.COOKIES['last_visit']
    #     # Cast the value to a Python date/time object.
    #     last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")
    #
    #     # If it's been more than a day since the last visit...
    #     if (datetime.now() - last_visit_time).seconds > 0:
    #         visits = visits + 1
    #         # ...and flag that the cookie last visit needs to be updated
    #         reset_last_visit_time = True
    # else:
    #     # Cookie last_visit doesn't exist, so flag that it should be set.
    #     reset_last_visit_time = True
    #
    #     conteudo['visits'] = visits
    #
    #     #Obtain our Response object early so we can add cookie information.
    # response = render(request, template, conteudo)
    #
    # if reset_last_visit_time:
    #     response.set_cookie('last_visit', datetime.now())
    #     response.set_cookie('visits', visits)
    #
    # # Return response back to the user, updating any cookies that need changed.
    #
    # return response

    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).seconds > 0:
            # ...reassign the value of the cookie to +1 of what it was before...
            visits = visits + 1
            # ...and update the last visit cookie, too.
            reset_last_visit_time = True
    else:
        # Cookie last_visit doesn't exist, so create it to the current date/time.
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits

    conteudo['visits'] = visits
    conteudo['controle_menu'] = 'home'


    response = render(request, template, conteudo)

    return response
