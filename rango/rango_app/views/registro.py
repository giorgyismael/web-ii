#coding:utf-8

from django.shortcuts import render
from django.contrib.auth import logout
from rango_app.forms import FormUsuario, FormUsuarioComplemento
from rango_app.models import Categoria
from rango_app.views.get_visits import get_visitante


def registro(request):
    template = 'rango/registro.html'
    registro = False

    if request.method != 'POST':
        if request.user:
            logout(request)
        form_usuario = FormUsuario()
        form_usuario_complento = FormUsuarioComplemento()

    else:

        form_usuario = FormUsuario(data=request.POST)
        form_usuario_complento = FormUsuarioComplemento(data=request.POST)

        if form_usuario.is_valid() and form_usuario_complento.is_valid():
            usuario = form_usuario.save()

            usuario.set_password(usuario.password)
            usuario.save()

            complemento = form_usuario_complento.save(commit=False)
            complemento.usuario = usuario

            if 'imagem' in request.FILES:
                complemento.picture = request.FILES['imagem']

            complemento.save()

            registro = True


    conteudo = {
        'form_usuario': form_usuario,
        'form_usuario_complento': form_usuario_complento,
        'titulo_pagina':'Rango - Cadastro',
        'titulos_h1':["Cadastre-se","Parabéns"],
        'registro':registro,
        "categorias" : Categoria.objects.order_by("-like")[:5],
        'visits':get_visitante(request),
        'controle_menu':'registro',

    }

    return render(request, template, conteudo)