#coding:utf-8
from django.shortcuts import render
from rango_app.views.get_visits import get_visitante
from rango_app.models import Categoria


def restricted(request):
    template = 'rango/restrito.html'
    conteudo = {
        "categorias" : Categoria.objects.order_by("-like")[:5],
        'visits':get_visitante(request),
        'titulo_pagina': 'Rango - Página Restrita'
    }
    return render(request, template, conteudo)
