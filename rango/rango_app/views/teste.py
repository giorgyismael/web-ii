#coding:utf-8

from django.shortcuts import render
from rango_app.models import Categoria

def teste(request):

    if request.session.get('visits'):
        visits = request.session.get('visits')
    else:
        visits = 0

    template = 'rango/teste.html'
    conteudo = {
        'titulo_pagina':'teste',
        "categorias" : Categoria.objects.order_by("-like")[:5],
        'controle_menu':'home',
        'visits':visits,
    }

    return render(request, template, conteudo)