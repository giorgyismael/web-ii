#coding:utf-8

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from rango_app.views.get_visits import get_visitante
from rango_app.models import Categoria

def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        #Tentar Autenticar
        usuario = authenticate(username=username, password=password)

        #Se usuario estiver com Usuario e senha Coretos, Seguimos adiante
        if usuario:

            #Verificar Se usuario Está ATIVO NO SISTEMA
            if usuario.is_active:

                # Se tudo der certo, utilizamos a FUNÇÃO  LOGIN.
                login(request, usuario)
                return HttpResponseRedirect('/rango/')

            #Se usuario estiver INATIVO, encaminha para página abaixo
            else:
                template = 'rango/conta_desabilitada.html.html'
                return render(request, template , {'titulo_pagina': 'Rango - Conta Desabilitada '})

        #Se usuario Estiver incorreto
        else:
            template = 'rango/login_invalido.html'
            conteudo = {
            'titulo_pagina':'Rango - Login Inválido',
            'controle_menu':'login_invalido',
            'visits':get_visitante(request),
    }

            return render(request, template, conteudo)

    else:
        #Carrega a pagina  de login
        template = 'rango/login.html'

    conteudo = {
        'titulo_pagina':'Login',
        'controle_menu':'login',
        "categorias" : Categoria.objects.order_by("-like")[:5],
        'visits':get_visitante(request),
    }

    return render(request, template, conteudo)