#coding:utf-8
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib.auth import  logout


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')
