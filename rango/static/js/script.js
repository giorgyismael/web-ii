function Menu(){
    try {
        pagina = document.getElementById("controle_menu").value;
        document.getElementById(pagina).className = 'item active';
    }
    catch(err) {
        document.getElementById("error_js").value = err.message;
    }
}

function PositionFooter() {
    var $footer = $("#footer"), footerHeight = $footer.height(),
    footerTop = ($(window).scrollTop() + $(window).height() - footerHeight) + "px";

    if (($(document.body).height() + footerHeight) < $(window).height()) {
        $footer.css({ position: "absolute", top: footerTop }); }
    else { $footer.css({ position: "absolute" }); }

    $footer.fadeTo(1000, 0.8);
}

function GerenciarFooter() {
    $(window).resize(PositionFooter)
    PositionFooter();
}
